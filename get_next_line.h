/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 11:51:32 by lorenuar          #+#    #+#             */
/*   Updated: 2020/05/25 16:48:25 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

/*
** ***** **
** MACRO **
** ***** **
*/
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 64
# endif

/*
** ******* **
** INCLUDE **
** ******* **
*/
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <limits.h>

# include "wraloc.h"

/*
** ********* **
** FUNCTIONS **
** ********* **
*/

int			get_next_line(int fd, char **line);
ssize_t		join_line(int fd, char **buf, char ***line);
size_t		hasto(char *s, char c);
char		*jointo(char *s1, char *s2, char **tofree);
void 		*free_s(char **p);
void 		*clean_malloc(size_t count, size_t size);

void 		printchars(char *s, size_t len);

#endif
