NAME = gnl

CFLAGS	= -Wall -Wextra -Wextra -Werror -g
CFLAGS	+= -D BUFFER_SIZE=1
# CFLAGS 	+= -D WRAP=0
# CFLAGS  += -D NO_VERBOSE=1
CFLAGS 	+= -fsanitize=address

SRC = \
		get_next_line.c \
		main.c

all: $(SRC)
	rm -f $(NAME) && $(CC) $(CFLAGS) $(SRC) -o $(NAME)
	# ./$(NAME)
	./$(NAME) a
	./$(NAME) a b 
	./$(NAME) b
	./$(NAME) c
	./$(NAME) a c
	
val:
	rm -f $(NAME) && $(CC) $(CFLAGS) $(SRC) -o $(NAME)

	valgrind ./$(NAME) a
