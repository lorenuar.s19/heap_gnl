/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 12:17:03 by lorenuar          #+#    #+#             */
/*   Updated: 2020/05/23 22:08:40 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <stdio.h>
#include "get_next_line.h"

#define PLINE(FD, ret)                                                    \
	printf("=== MAIN === | R %d | fd %d | Line \"%s\"\n", ret, FD, line); \
	if (line) { free(line); line = NULL; }

int main(int argc, char *argv[])
{
	int fd0;
	int fd1;
	int ret1;
	int ret2;
	char *line;

	fd0 = 0;
	fd1 = 0;
	line = NULL;
	if (argc == 2)
	{
		fd0 = open(argv[1], O_RDONLY);
	}
	if (argc <= 2)
	{
		while ((ret1 = get_next_line(fd0, &line)) > 0)
		{
			PLINE(fd0, ret1);
			// _get_summary();
		}
		PLINE(fd0, ret1);
	}
	if (argc == 3)
	{
		fd0 = open(argv[1], O_RDONLY);
		fd1 = open(argv[2], O_RDONLY);
		ret1 = 1;
		ret2 = 1;
		while (ret1 > 0 && ret2 > 0)
		{
			if ((ret1 = get_next_line(fd0, &line)) > 0)
			{
				PLINE(fd0, ret1);
			}
			if ((ret2 = get_next_line(fd1, &line)) > 0)
			{
				PLINE(fd1, ret2);
			}
		}
		PLINE(fd0, ret1);
		PLINE(fd1, ret2);
	}
	return (0);
}
