/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 11:53:10 by lorenuar          #+#    #+#             */
/*   Updated: 2020/05/25 16:52:41 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

#define P_NUM(x) printf("| "#x" %ld |\n", x)

int				get_next_line(int fd, char **line)
{
	static char	*buf[FOPEN_MAX];
	ssize_t		ret;
	int			i;

	ret = 1;
	if (BUFFER_SIZE < 1 || fd < 0 || fd > FOPEN_MAX || !line)
	{
		return (printf("INVALID INPUT\n"), -1);
	}
	if (!(*line = jointo(buf[fd], NULL, NULL)))
	{
		return (printf("JOINTO MALLOC ERROR\n"), -1);
	}
	if (!buf[fd] && !(buf[fd] = (char *)clean_malloc(BUFFER_SIZE + 1, sizeof(char))))
	{
		return (printf("CANNOT ALLOCATE BUFFER\n"), -1);
	}
	printf(" BFORE GNL | buf[fd] '%p'\n", buf[fd]);
	while ((!hasto(buf[fd], '\n')) && ret)
	{
		if (buf[fd] && (ret = read(fd, &buf[fd], BUFFER_SIZE)) == -1)
		{
			return (printf("READ ERROR\n"),-1);
		}
		buf[fd] -= ret;
		printf("AFTER GNL | buf[fd] '%p'\n", buf[fd]);
		printchars(buf[fd], ret);
		
		if (line && *line && buf[fd] && !(*line = jointo(*line, buf[fd], line)))
		{
			return (printf("LOOPING JOINTO ERROR\n"),-1);	
		}
	}
	if (!hasto(buf[fd], '\n'))
	{
		free_s(&buf[fd]);
		return (0);
	}
	i = 0;
	while (buf[fd] && (ret = hasto(buf[fd], '\n')))
	{
		buf[fd][i++] = buf[fd][ret];
	}
	buf[fd][i] = '\0';
	return (1);
}

size_t			hasto(char *s, char c)
{
	size_t		to;

	to = 0;
	while (s && s[to])
	{
		printf("HS | s[%lu] > %d > '%c'\n", to, s[to], s[to]);
		if (s[to] == c)
		{
			return (to + 1);
		}
			to++;
	}
	if (s && s[to] == c)
	{
		return (to + 1);
	}
	return (0);
}

char			*jointo(char *s1, char *s2, char **tofree)
{
	char		*a;
	size_t		sl1;
	size_t		sl2;
	size_t		i;
	size_t		j;

	i = 0;
	j = 0;
	a = NULL;
	sl1 = hasto(s1, '\0');	
	sl2 = hasto(s2, '\0');
	if (!(a = (char *)clean_malloc(sl1 + sl2 + 1, sizeof(char))) &&
		!free_s(tofree))
	{
		return (NULL);
	}
	while (s1 && s1[j] && s1[j] != '\n')
		a[i++] = s1[j++];
	j = 0;
	while (s2 && s2[j] && s2[j] != '\n')
		a[i++] = s2[j++];
	a[i] = '\0';
	free_s(tofree);
	return (a);
}

void			*free_s(char **p)
{
	if (p && *p)
	{
		free(*p);
		*p = NULL;
		return (*p);
	}
	return (NULL);
}

void 			*clean_malloc(size_t count, size_t size)
{
	size_t		i;
	void		*data;
	char 		*tmp;

	if(!(data = malloc(count * size)))
		return (NULL);
	i = 0;
	tmp = (char *)data;
	while (tmp && i < (count * size))
	{
		tmp[i++] = 0;
	}
	printchars(data, count *size);
	return (data);
}

void	printchars(char *s, size_t len)
{
	size_t i;

	i = 0;
	printf("=*=*=*=*=*=*=*=*=*=*=\nChars : \n");
	if (!s)
		printf("NULL\n");
	while (s && i < len)
	{
		if (s[i] == '\0')
		{
			printf("[\\0]");
			if (++i < len)
				printf(",");
		}
		else if (s[i] == '\n')
		{
			printf("[\\n]");
			if (++i < len)
				printf(",\n");
		}
		else
		{
			printf("[%c]", s[i]);
			if (++i < len)
				printf(", ");
		}
	}
	printf("\n=*=*=*=*=*=*=*=*=*=*=\n");
}
